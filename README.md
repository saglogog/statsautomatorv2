# ShavingStatsAutomator

*Automates the extraction of statistics from xml files*

## Information extraction and data transformation

Simple automation tool that uses the xml.etree.ElementTree python module 
to extract information from xml files and/or modify them.

The data is then transformed to extract useful statistics.

## Interface

Also contains a useful interface built using the cross-platform kivy library.