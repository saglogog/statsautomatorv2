#!/usr/bin/env python
import sys
import re
import json
import xml.etree.ElementTree as ET
from collections import deque


class CustomKVALabelValidator:
    """
    Validates the Shaving Phases' Labels  and the Strokes' Labels
    formatting and sequence as much as possible
    """

    def __init__(self, filename, tree):
        # Declare lists that contain the phase names
        self.phase_names = ["ShavePr", "EndShavePr", "FoamApp", "EndFoamApp",
                            "FoamRins", "EndFoamRins", "EndRins", "Shaving", "EndShaving"]
        self.rz_rins_nm = ["RinsSimple", "RinsFR", "RinsKnock"]

        self.fname = filename
        # the tree is parsed outside the class so as to not parse the tree
        # for each object and the different classes that need it
        self.tree = tree
        # get root element
        self.root = self.tree.getroot()
        # get all keyframe titles
        self.keyframeTitles = self.root.findall("./Keyframes/Keyframe/Title")
        # get all stroke labels
        self.trackLabels = self.root.findall("./Tracks/Track/Label/Text")

    def print_keyframe_titles(self):
        # for title in self.keyframeTitles:
        #   print title.text
        # return self.keyframeTitles
        return json.JSONEncoder().encode({'Keyframe Titles': [kt.text for kt in self.keyframeTitles]})

    def print_stroke_labels(self):
        # for labelText in self.trackLabels:
        #     print labelText.text
        # return self.trackLabels
        return json.JSONEncoder().encode({'Track Labels': [tl.text for tl in self.trackLabels]})

    def validate_phase_name(self, keyframe_title):
        """
        Check that the name of each phase is in correct format,
        ie : 2 RinsFR 0:02:33:45
        """
        validator = False

        for item in self.rz_rins_nm:
            pattern1 = r"[0-9]{1,} " + item + \
                " [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
            if re.match(pattern1, keyframe_title):
                validator = True

        for item in self.phase_names:
            pattern2 = r"^" + item + " [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
            if re.match(pattern2, keyframe_title):
                validator = True

        if not validator:
            print 'PHASE NAME MISMATCH: {0}'.format(keyframe_title)
            return 'PHASE NAME MISMATCH: {0}'.format(keyframe_title)

    def validate_all_phase_names(self):
        err_phase_names = []
        for title in self.keyframeTitles:
            if self.validate_phase_name(title.text):
                err_phase_names.append(self.validate_phase_name(title.text))
        return json.JSONEncoder().encode({'Wrong Phase Names': err_phase_names})

    def validate_stroke_name(self, track_label):
        test_string = "358, Against, Pen, Knee-Back, Up, 0:08:17:00"
        pattern = r"[0-9]{1,}, (Against|With), (Pen|Pincer|Flute|Spoon|Shovel|Feather), (AnkleR|AnkleL|Low-FrontR|Low-FrontL|Low-BackR|Low-BackL|Knee-FrontR|Knee-FrontL|Knee-BackR|Knee-BackL|Up-FrontR|Up-FrontL|Up-BackR|Up-BackL), (Up|Sit), [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        p = re.compile(pattern)
        if not p.match(track_label):
            print 'STROKE NAME MISMATCH: {0}'.format(track_label)
            return 'STROKE NAME MISMATCH: {0}'.format(track_label)

    def validate_all_stroke_names(self):
        err_stroke_names = []
        for labelText in self.trackLabels:
            if self.validate_stroke_name(labelText.text):
                err_stroke_names.append(
                    self.validate_stroke_name(labelText.text))
        return json.JSONEncoder().encode({'Wrong Stroke Names': err_stroke_names})

    def validate_stroke_sequence(self, modify_select):
        """
        Receives a list of track labels and checks if their serial number
        is in increasing order. If modify_select == 1 the method also
        tries to correct the stroke sequence.
        """
        has_file_been_modified = False
        err_stroke_no = []
        i = 1
        for lbl in self.trackLabels:
            fields = lbl.text.split(',')
            if int(fields[0]) != i:
                # print 'SERIAL No MISMATCH: {0}'.format(lbl.text)
                # print fields[0], i
                err_stroke_no.append('SERIAL No MISMATCH: {0}'.format(lbl.text)
                                     + fields[0] + ', ' + str(i))
                if (modify_select == 1):
                    new_stroke_name = self.correct_stroke_sequence(fields, i)
                    lbl.text = new_stroke_name
                    has_file_been_modified = True
            i = i + 1
        if has_file_been_modified == True:
            self.tree.write(self.fname + "2")
        # return err_stroke_no
        return json.JSONEncoder().encode({'Wrong Stroke Nos': err_stroke_no})

    def correct_stroke_sequence(self, fields, i):
        """
        If a serial no mismatch is detected this method corrects the
        wrong serial no.
        """
        fields[0] = str(i)
        new_stroke_name = ",".join(fields)
        # print new_stroke_name
        return new_stroke_name

    def validate_stroke_times(self):
        """
        This method checks the track labels and ensures there are no duplicate
        times.
        """
        # Sort the times in a list and then check one by one the times with the
        # next on list. Then, even if there are more than two equal times, they
        # will be discovered without multiple iterations.
        stroke_times = [s.text.split()[-1] for s in self.trackLabels]
        stroke_times.sort()
        stroke_times = [x for x, y in zip(stroke_times, stroke_times[1:])
                        if x == y]
        # return stroke_times
        return json.JSONEncoder().encode({'Duplicate stroke times': stroke_times})

    def validate_phase_times(self):
        """
        Split the phase name in time and name, get the time and check all the
        times against each other to establish if time duplication exists in phase
        names. This method sorts the times so that we only have to check each
        time against the next one.
        """
        # get only the times in the text portion of the phase name w/ list
        # comprehension
        title_times = [s.text.split()[-1] for s in self.keyframeTitles]
        title_times.sort()
        # get the duplicate times using zip method to get a tuple of the two
        # adjacent elements each time and list comprehension to check if they
        # are equal
        title_times = [x for x, y in zip(title_times, title_times[1:])
                       if x == y]
        # alternative implementation
        # problematic_times = []
        # prev_t = ""
        # for t in title_times:
        #     if t == prev_t:
        #         problematic_times.append(t)
        #     prev_t = t

        # return title_times
        return json.JSONEncoder().encode({'Duplicate phase times': title_times})

    def extract_phase_names(self):
        """
        This method gets the phase names and razor rinsing episode names from
        the keyframeTitle which is follows a pattern of type 'phasename time'
        or '# rinsepisodename time' and replaces the razor rinsing episode names
        with Rins so that the validate_end_phases() method can validate the names.
        ATTENTION: Need to validate the phase names first to make sure they
                   are written correctly!
        """
        phaseNames = []
        for item in self.keyframeTitles:
            # check if there is a number before the phase (if the phase is
            # a rinsing episode) If so, get the rinsing episode name.
            if item.text.split()[0] in self.phase_names:
                phaseName = item.text.split()[0]
            else:
                phaseName = item.text.split()[1]
            if phaseName in self.rz_rins_nm:
                phaseName = "Rins"
            phaseNames.append(phaseName)
        return phaseNames

    def check_if_phase_is_end_phase(self, phase_name):
        """
        We assume that a phase is ended with a name that starts w/ the End- prefix
        """
        if phase_name[0] + phase_name[1] + phase_name[2] == "End":
            return True
        else:
            return False

    def validate_end_phases(self):
        """
        Each phase must have a corresponding end phase aprepended with
        the end word(ShavePr -> EndShavePr).
        This method uses a queue to store all the phases and one by one matches
        the beginning phases with their closest corresponding end phases and
        deletes them both from the queue.
        ATTENTION:  Need to validate the phase names first to make sure they are written correctly!
        ATTENTION:  If you can put the elements in order based their times please do before
                    validating the end phases!
        """
        # get the keyframe titles text
        keyframeTitlesText = self.extract_phase_names()
        # create a queue
        queue = deque(keyframeTitlesText)
        erroneous_phases = []
        try:
            for i in range(0, len(keyframeTitlesText) - 1):
                # check if there are still elements in the queue
                # because we also erase the end phases w/ queue.remove()
                if queue:
                    phaseName = queue.popleft()
                    if not self.check_if_phase_is_end_phase(phaseName):
                        erroneous_phases.append(phaseName)
                        for j in range(i + 1, len(keyframeTitlesText) - 1):
                            if phaseName == ''.join(keyframeTitlesText[j][3:]):
                                erroneous_phases.remove(phaseName)
                                queue.remove(keyframeTitlesText[j])
                                break
                    else:
                        # we shouldn't be encountering end phases
                        erroneous_phases.append(phaseName)
                else:
                    break
            # check if dequeue empty
            if queue:
                raise ValueError('The queue should be empty by now!')
        except ValueError():
            raise
        # return erroneous_phases
        return json.JSONEncoder().encode({'Wrong End Phases (Please validate phase names and put elements in order first)': erroneous_phases})


def main():
    # get filename as argument
    filename = sys.argv[1]
    # parse
    tree = ET.parse(filename)

    # for child in root:
    #  print child.tag, child.attrib, child.text

    try:
        c = CustomKVALabelValidator(filename, tree)
        c.print_keyframe_titles()
        c.print_stroke_labels()
        c.validate_all_phase_names()
        c.validate_all_stroke_names()
        c.validate_stroke_sequence(0)
    except:
        raise


if __name__ == '__main__':
    main()
