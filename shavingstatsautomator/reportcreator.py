import json


class ReportCreator():
    """
    Prints the shaving stats report using the arguments supplied.
    """

    def __init__(self):
        pass

    def print_report(self, filename, args):
        """
        Prints the report.
        """
        rep_name = './report_' + filename + '.txt'

        f = open(rep_name, 'w+')
        s = "REPORT for file(s) " + filename
        f.write(s.center(80) + '\n')
        f.write("====================================".center(80) + "\n\n\n\n")
        for a in args:
            dec = json.JSONDecoder().decode(a)
            key = dec.keys()[0]
            f.write(key.center(80) + "\n")
            f.write("--------------------".center(80) + "\n")
            # check if the object is a list and handle appropriately
            if not isinstance(dec[key], basestring):
                for a in dec[key]:
                    try:
                        f.write(a.center(80) + "\n")
                    except AttributeError:
                        # if the object is not a simple list but a dict we get
                        # an attribute error.
                        f.write(
                            (str(a.keys()[0]) + ":" + str(a[a.keys()[0]])).center(80) + "\n")
            else:
                f.write(str(dec[key]).center(80) + "\n")
            f.write(
                "\n\n" + "====================================".center(80) + "\n\n\n")
        f.close()
# ------------------------------------
