#!/usr/bin/env python
import sys
import re
import json
import xml.etree.ElementTree as ET
from decimal import Decimal


class SeparateLegCalculator:
    """
    Makes the calculations made in phases calculator for each leg separately.
    """

    def __init__(self, tree):
        self.tree = tree
        # get root element
        self.root = self.tree.getroot()
        # get all keyframe titles
        self.keyframeTitles = self.root.findall("./Keyframes/Keyframe/Title")
        # get all Track labels
        self.trackLabels = self.root.findall("./Tracks/Track/Label/Text")
        # get all tracks
        self.tracks = self.root.findall("./Tracks/Track")
        # separator time here is a list containing both the time and the first leg ie [23588, 'R']
        self.separator_time = self.get_separator_time()

    def print_keyframe_titles(self):
        for title in self.keyframeTitles:
          print title.text

    def print_stroke_labels(self):
        for labelText in self.trackLabels:
            print labelText.text

    def calculate_in_hundrendths(self, hours, mins, secs, hundrendths):
        converted_secs = secs * 100
        converted_mins = mins * 60 * 100
        converted_hours = hours * 60 * 60 * 100
        hundrendths = hundrendths + converted_secs + converted_mins + converted_hours
        return hundrendths

    def get_separator_time(self):
        """
        Searches through all the tracks labels and finds the one greatest time value
        before the change of legs (it is assumed that the subject shave one leg
        after the other without changing skipping contiously from leg to leg).
        """
        times_left_leg = []
        times_right_leg = []
        for tr in self.trackLabels:
            label_split = re.split(', ', tr.text)
            if label_split[3][-1] == 'R':
                times_right_leg.append(self.calculate_in_hundrendths(int(label_split[5].split(
                    ':')[0]), int(label_split[5].split(':')[1]), int(label_split[5].split(':')[2]), int(label_split[5].split(':')[3])))
            elif re.split(', ', tr.text)[3][-1] == 'L':
                times_left_leg.append(self.calculate_in_hundrendths(int(label_split[5].split(
                    ':')[0]), int(label_split[5].split(':')[1]), int(label_split[5].split(':')[2]), int(label_split[5].split(':')[3])))
            else:
                raise ValueError(
                    "The strokes probably don't contain leg letter.")
        times_left_leg.sort()
        times_right_leg.sort()
        separator_time = []
        if times_left_leg[0] > times_right_leg[0]:
            separator_time.append(times_right_leg[-1])
            separator_time.append('R')
        else:
            separator_time.append(times_left_leg[-1])
            separator_time.append('L')
        # print times_right_leg[0]
        # print times_right_leg [-1]
        # print times_left_leg[0]
        # print times_left_leg [-1]
        # print separator_time
        return separator_time

    def find_shaving_phase_duration(self):
        """
        Find gross shaving phase duration for each leg.
        """
        shav_pattern = r"^Shaving [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        shav_end_pattern = r"^EndShaving [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        shav_dur = []
        for i in range(0, len(self.keyframeTitles) - 1):
            if re.match(shav_pattern, self.keyframeTitles[i].text):
                # print self.keyframeTitles[i].text
                shav_begin = self.keyframeTitles[i].text.split(' ')[1]
                # print shav_begin
                for title in self.keyframeTitles[i + 1:]:
                    if re.match(shav_end_pattern, title.text):
                        # print title.text
                        shav_end = title.text.split(' ')[1]
                        # print shav_end
                        break
                split_shav_beg = shav_begin.split(':')
                split_shav_end = shav_end.split(':')
                shav_beg_in_hun = self.calculate_in_hundrendths(int(split_shav_beg[0]), int(
                    split_shav_beg[1]), int(split_shav_beg[2]), int(split_shav_beg[3]))
                shav_end_in_hun = self.calculate_in_hundrendths(int(split_shav_end[0]), int(
                    split_shav_end[1]), int(split_shav_end[2]), int(split_shav_end[3]))
                shav_dur_first = self.separator_time[0] - shav_beg_in_hun
                shav_dur_sec = shav_end_in_hun - self.separator_time[0]
                # print shav_dur_first
                # print shav_dur_sec
                break
        shav_dur.append(shav_dur_first)
        shav_dur.append(shav_dur_sec)
        print "Shaving Phase duration until separator time (in hundreths of secs): "
        print shav_dur[0]
        print "Shaving Phase duration after separator time (in hundreths of secs): "
        print shav_dur[1]
        return shav_dur

    def find_total_razor_rinsing_episodes(self):
        rz_rins_pattern = r"[0-9]{1,} (RinsSimple|RinsFR|RinsKnock) [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        rz_end_rins_pattern = r"^EndRins [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        overall_rz_rins_dur_first = 0
        overall_rz_rins_dur_sec = 0
        overall_rz_rins_dur = []
        split_stroke_begin = []
        split_stroke_end = []
        # for title in self.keyframeTitles:
        for i in range(0, len(self.keyframeTitles) - 1):
            if re.match(rz_rins_pattern, self.keyframeTitles[i].text):
                # print self.keyframeTitles[i].text
                stroke_begin = self.keyframeTitles[i].text.split(' ')[2]
                # print stroke_begin
                for title in self.keyframeTitles[i + 1:]:
                    if re.match(rz_end_rins_pattern, title.text):
                        # print title.text
                        stroke_end = title.text.split(' ')[1]
                        # print stroke_end
                        break
                split_stroke_beg = stroke_begin.split(':')
                split_stroke_end = stroke_end.split(':')
                str_beg_in_hun = self.calculate_in_hundrendths(int(split_stroke_beg[0]), int(
                    split_stroke_beg[1]), int(split_stroke_beg[2]), int(split_stroke_beg[3]))
                str_end_in_hun = self.calculate_in_hundrendths(int(split_stroke_end[0]), int(
                    split_stroke_end[1]), int(split_stroke_end[2]), int(split_stroke_end[3]))
                # print str_beg_in_hun
                if str_beg_in_hun < self.separator_time[0]:
                    overall_rz_rins_dur_first = overall_rz_rins_dur_first + \
                        (str_end_in_hun - str_beg_in_hun)
                else:
                    overall_rz_rins_dur_sec = overall_rz_rins_dur_sec + \
                        (str_end_in_hun - str_beg_in_hun)
        overall_rz_rins_dur.append(overall_rz_rins_dur_first)
        overall_rz_rins_dur.append(overall_rz_rins_dur_sec)
        print "Overall rinsing duration for first leg: "
        print overall_rz_rins_dur[0]
        print "Overall rinsing duration for second leg: "
        print overall_rz_rins_dur[1]

    def find_total_actual_shaving(self):
        """
        Find total actual shaving for each leg.
        """
        # time duration for left leg
        time_dur_list_fir = [0, 0, 0, 0]
        # time duration for right leg
        time_dur_list_sec = [0, 0, 0, 0]
        for tr in self.tracks:
            # get stroke time and convert to compare to sepaprator time
            label_time = re.split(', ', tr.find("Label/Text").text)[5]
            time_split = label_time.split(':')
            time_in_hun = self.calculate_in_hundrendths(int(time_split[0]), int(time_split[1]), int(time_split[2]), int(time_split[3]))
            # get stroke duration from corresponding xml field in kva
            track_pos_count = tr.find("TrackPositionList").get("Count")
            duration_of_stroke = tr.findall(
                "TrackPositionList/TrackPosition")[int(track_pos_count) - 1].get("UserTime")
            split_string = duration_of_stroke.split(':')
            #compare to separator time
            if time_in_hun < self.separator_time[0]:
                # add the times in their corresponding list items so that we can convert them later
                time_dur_list_fir[0] = time_dur_list_fir[0] + int(split_string[0])
                time_dur_list_fir[1] = time_dur_list_fir[1] + int(split_string[1])
                time_dur_list_fir[2] = time_dur_list_fir[2] + int(split_string[2])
                time_dur_list_fir[3] = time_dur_list_fir[3] + int(split_string[3])
            else:
                time_dur_list_sec[0] = time_dur_list_sec[0] + int(split_string[0])
                time_dur_list_sec[1] = time_dur_list_sec[1] + int(split_string[1])
                time_dur_list_sec[2] = time_dur_list_sec[2] + int(split_string[2])
                time_dur_list_sec[3] = time_dur_list_sec[3] + int(split_string[3])
        # calculate time in hundrendths of seconds with the help of the method
        act_shav_time_fir = self.calculate_in_hundrendths(int(time_dur_list_fir[0]), int(time_dur_list_fir[1]), int(time_dur_list_fir[2]), int(time_dur_list_fir[3]))
        act_shav_time_sec = self.calculate_in_hundrendths(int(time_dur_list_sec[0]), int(time_dur_list_sec[1]), int(time_dur_list_sec[2]), int(time_dur_list_sec[3]))
        # print times in hundreths of seconds
        print "Actual shaving time for the first leg:"
        print act_shav_time_fir
        print "Actual shaving time for the second leg:"
        print act_shav_time_sec

    def find_number_of_rinsing_episodes(self):
        rins_eps_first = [s.text.split()[1] for s in self.keyframeTitles
                          if s.text.split()[1] in ('RinsSimple', 'RinsFR', 'RinsKnock') if s.text.split()[2]
                          if self.calculate_in_hundrendths(int(s.text.split()[2].split(':')[0]), int(s.text.split()[2].split(':')[1]),
                           int(s.text.split()[2].split(':')[2]), int(s.text.split()[2].split(':')[3])) < self.separator_time[0]]
        print "Rinsing episodes for the first leg:"
        print rins_eps_first
        rins_eps_sec = [s.text.split()[1] for s in self.keyframeTitles
                        if s.text.split()[1] in ('RinsSimple', 'RinsFR', 'RinsKnock') if s.text.split()[2]
                        if self.calculate_in_hundrendths(int(s.text.split()[2].split(':')[0]), int(s.text.split()[2].split(':')[1]),
                         int(s.text.split()[2].split(':')[2]), int(s.text.split()[2].split(':')[3])) > self.separator_time[0]]
        print "Rinsing episodes for the second leg:"
        print rins_eps_sec
        return [rins_eps_first, rins_eps_sec]

    def find_how_many_of_each_type_of_rinsing_episode(self, rins_eps):
        """
        Calculates the total # of rinsing episodes of each type of rinsing
        episode
        """
        rins_simple_count = 0
        rins_fr_count = 0
        rins_knock_count = 0
        for item in rins_eps:
            if item == 'RinsSimple':
                rins_simple_count += 1
            elif item == 'RinsFR':
                rins_fr_count += 1
            elif item == 'RinsKnock':
                rins_knock_count += 1
            else:
                raise ValueError("The value was not one of the expected but the following: " + str(item))
        # get the counters of all the rins episodes in a list and return the
        # list
        rins_ep_types = []
        print "Rins Simple Count: " + str(rins_simple_count)
        print "Rins FR Count: " + str(rins_fr_count)
        print "Rins Knock Count: " + str(rins_knock_count)


def main():
    args_len = len(sys.argv)
    for i in range(0, args_len - 1):
        # get filename as argument
        filename = sys.argv[i+1]
        # parse
        tree = ET.parse(filename)

        # for child in root:
        #  print child.tag, child.attrib, child.text

        try:
            print "\n\n"
            print filename
            print "\n\n"
            s = SeparateLegCalculator(tree)
            s.print_keyframe_titles()
            print "\n\n"
            s.print_stroke_labels()
            print "\n\n\n"
            print "Leg that is shaved first, hereby called first leg: " + s.separator_time[1]
            s.find_shaving_phase_duration()
            s.find_total_razor_rinsing_episodes()
            s.find_total_actual_shaving()
            rins_eps = s.find_number_of_rinsing_episodes()
            print "Number of rinsing episodes for first leg:"
            s.find_how_many_of_each_type_of_rinsing_episode(rins_eps[0])
            print "Number of rinsing episodes for second leg:"
            s.find_how_many_of_each_type_of_rinsing_episode(rins_eps[1])
        except:
            raise


if __name__ == '__main__':
    main()
