#!/usr/bin/env python
import sys
import re
import json
import xml.etree.ElementTree as ET
from decimal import Decimal
from datetime import datetime


class PhasesCalculator:
    """
    We need to calculate:
    1) The total razor rinsing episodes' time
    2) Total shaving (-ShavePrep)
    3) Total actual shaving (Sum of strokes)
    4) Number of rinsing episodes
    5) Number of each type of rinsing episodes
    """

    def __init__(self, tree):
        self.tree = tree
        # get root element
        self.root = self.tree.getroot()
        # get all keyframe titles
        self.keyframeTitles = self.root.findall("./Keyframes/Keyframe/Title")
        # get all Tracks
        self.tracks = self.root.findall("./Tracks/Track")

    def calculate_in_hundrendths(self, hours, mins, secs, hundrendths):
        converted_secs = secs * 100
        converted_mins = mins * 60 * 100
        converted_hours = hours * 60 * 60 * 100
        hundrendths = hundrendths + converted_secs + converted_mins + converted_hours
        return hundrendths

    def find_shaving_phase_duration(self):
        """
        Simply calculate shaving phase duration by
        subtracting shaving phase beginning from end
        """
        shav_pattern = r"^Shaving [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        shav_end_pattern = r"^EndShaving [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        shav_dur = 0
        for i in range(0, len(self.keyframeTitles) - 1):
            if re.match(shav_pattern, self.keyframeTitles[i].text):
                print self.keyframeTitles[i].text
                shav_begin = self.keyframeTitles[i].text.split(' ')[1]
                print shav_begin
                for title in self.keyframeTitles[i + 1:]:
                    if re.match(shav_end_pattern, title.text):
                        print title.text
                        shav_end = title.text.split(' ')[1]
                        print shav_end
                        break
                split_shav_beg = shav_begin.split(':')
                split_shav_end = shav_end.split(':')
                shav_beg_in_hun = self.calculate_in_hundrendths(int(split_shav_beg[0]), int(
                    split_shav_beg[1]), int(split_shav_beg[2]), int(split_shav_beg[3]))
                shav_end_in_hun = self.calculate_in_hundrendths(int(split_shav_end[0]), int(
                    split_shav_end[1]), int(split_shav_end[2]), int(split_shav_end[3]))
                shav_dur = shav_end_in_hun - shav_beg_in_hun
                print shav_dur
                break
        print "The total duration of shaving is: " + str(Decimal(shav_dur) / 100) + " seconds."
        # return str(Decimal(shav_dur)/100)
        return json.JSONEncoder().encode({"Total duration of shaving": str(Decimal(shav_dur) / 100)})

    def find_total_razor_rinsing_episodes(self):
        """
        Sum of razor rinsing episodes
        """
        rz_rins_pattern = r"[0-9]{1,} (RinsSimple|RinsFR|RinsKnock) [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        rz_end_rins_pattern = r"^EndRins [0-9]{1}:[0-9]{2}:[0-9]{2}:[0-9]{2}"
        overall_rz_rins_dur = 0
        split_stroke_begin = []
        split_stroke_end = []
        # for title in self.keyframeTitles:
        for i in range(0, len(self.keyframeTitles) - 1):
            if re.match(rz_rins_pattern, self.keyframeTitles[i].text):
                # print self.keyframeTitles[i].text
                stroke_begin = self.keyframeTitles[i].text.split(' ')[2]
                print stroke_begin
                for title in self.keyframeTitles[i + 1:]:
                    if re.match(rz_end_rins_pattern, title.text):
                        # print title.text
                        stroke_end = title.text.split(' ')[1]
                        print stroke_end
                        break
                split_stroke_beg = stroke_begin.split(':')
                split_stroke_end = stroke_end.split(':')
                str_beg_in_hun = self.calculate_in_hundrendths(int(split_stroke_beg[0]), int(
                    split_stroke_beg[1]), int(split_stroke_beg[2]), int(split_stroke_beg[3]))
                str_end_in_hun = self.calculate_in_hundrendths(int(split_stroke_end[0]), int(
                    split_stroke_end[1]), int(split_stroke_end[2]), int(split_stroke_end[3]))
                overall_rz_rins_dur = overall_rz_rins_dur + \
                    (str_end_in_hun - str_beg_in_hun)
        print "The total duration of seconds of razor rinsing is: " + str(Decimal(overall_rz_rins_dur) / 100) + " seconds."
        # return str(Decimal(overall_rz_rins_dur)/100)
        return json.JSONEncoder().encode({"Total duration of razor rinsing": str(Decimal(overall_rz_rins_dur) / 100)})

    def find_number_of_rinsing_episodes(self):
        """
        Calculates the total # of rinsing episodes
        ATTENTION: Validate phase names first bc the method requires the phase
                   to be formatted correctly.
        """
        # get all the rinsing episodes in a list, assuming of course that the
        # rins ep name its the second 'word' in a keyframetitle and that it's one
        # of the RinsSimple... etc
        rins_eps = [s.text.split()[1] for s in self.keyframeTitles
                    if s.text.split()[1] in ('RinsSimple', 'RinsFR', 'RinsKnock')]
        # return len(rins_eps)
        return json.JSONEncoder().encode({"No. of rinsing episodes (please validate phase names first)": str(len(rins_eps))})

    def find_how_many_of_each_type_of_rinsing_episode(self):
        """
        Calculates the total # of rinsing episodes of each type of rinsing
        episode
        """
        rins_eps = [s.text.split()[1] for s in self.keyframeTitles
                    if s.text.split()[1] in ('RinsSimple', 'RinsFR', 'RinsKnock')]
        rins_simple_count = 0
        rins_fr_count = 0
        rins_knock_count = 0
        for item in rins_eps:
            if item == 'RinsSimple':
                rins_simple_count += 1
            elif item == 'RinsFR':
                rins_fr_count += 1
            elif item == 'RinsKnock':
                rins_knock_count += 1
            else:
                raise ValueError("The value was not one of the expected but \
                    the following: " + item)
        # get the counters of all the rins episodes in a list and return the
        # list
        # rins_ep_types = []
        # rins_ep_types.append(rins_simple_count)
        # rins_ep_types.append(rins_fr_count)
        # rins_ep_types.append(rins_knock_count)
        # return rins_ep_types
        return json.JSONEncoder().encode({"Rinsing Episode Types": [{'Rinse Simple': rins_simple_count}, {'Rinse FR': rins_fr_count}, {'Rinse Knock': rins_knock_count}]})

    def find_total_actual_shaving(self):
        """
        Calculates the overall duration of strokes during shaving
        (Sum of stroke durations).
        """
        duration_of_strokes = 0
        time_dur_list = [0, 0, 0, 0]
        for tr in self.tracks:
            track_pos_count = tr.find("TrackPositionList").get("Count")
            duration_of_stroke = tr.findall(
                "TrackPositionList/TrackPosition")[int(track_pos_count) - 1].get("UserTime")
            # time = datetime.strptime(duration_of_stroke, "%H:%M:%S") --> no hundreth seconds in python format
            print duration_of_stroke
            # time string format: 0:05:25:76 w/ 0 being hour, 5 minutes, 25 seconds, 76 hundredth.
            split_string = duration_of_stroke.split(':')
            # add the times in their corresponding list items so that we can convert them later
            time_dur_list[0] = time_dur_list[0] + int(split_string[0])
            time_dur_list[1] = time_dur_list[1] + int(split_string[1])
            time_dur_list[2] = time_dur_list[2] + int(split_string[2])
            time_dur_list[3] = time_dur_list[3] + int(split_string[3])
        print "we have " + str(time_dur_list[0]) + " hours"
        print "we have " + str(time_dur_list[1]) + " mins"
        print "we have " + str(time_dur_list[2]) + " secs"
        print "we have " + str(time_dur_list[3]) + " hundredths of secs"
        print "we have " + str(time_dur_list[2] + Decimal(time_dur_list[3]) / 100) + " seconds of actual shaving!"
        # print time_dur_list[2] + Decimal(time_dur_list[3])/100
        # return str(time_dur_list[2] + Decimal(time_dur_list[3])/100)
        return json.JSONEncoder().encode({"Sum of stroke durations": str(time_dur_list[2] + Decimal(time_dur_list[3]) / 100)})


def main():
    # get filename as argument
    filename = sys.argv[1]
    # parse
    tree = ET.parse(filename)

    # for child in root:
    #  print child.tag, child.attrib, child.text

    try:
        p = PhasesCalculator(tree)
        p.find_total_actual_shaving()
        p.find_total_razor_rinsing_episodes()
        p.find_shaving_phase_duration()
    except:
        raise


if __name__ == '__main__':
    main()
