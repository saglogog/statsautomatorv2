#!/usr/bin/env python
import sys
import re
import json
import xml.etree.ElementTree as ET
from decimal import Decimal

def correct_stroke_sequence_L(fields):
    fields[3] = fields[3] + 'L'
    new_stroke_name = ",".join(fields)
    return new_stroke_name

def correct_stroke_sequence_R(fields):
    fields[3] = fields[3] + 'R'
    new_stroke_name = ",".join(fields)
    return new_stroke_name

def calculate_in_hundrendths(hours, mins, secs, hundrendths):
    converted_secs = secs * 100
    converted_mins = mins * 60 * 100
    converted_hours = hours * 60 * 60 * 100
    hundrendths = hundrendths + converted_secs + converted_mins + converted_hours
    return hundrendths

# convert the string of time to a list of integers("0:02:18:16" -> [0, 2, 18, 16] )
def get_ints_from_time_string(time_string):
    str_list = time_string.split(':')
    int_list=[]
    for i in str_list:
        int_list.append(int(i))
    return int_list

def main():

    try:
        # get time that separated left from right leg strokes as argument
        time = sys.argv[2]
        # get first leg as arguement
        f_leg = sys.argv[3]
        # get filename as argument
        filename = sys.argv[1]
        # parse
        tree = ET.parse(filename)

        # get root element
        root = tree.getroot()
        # get all keyframe titles
        keyframeTitles = root.findall("./Keyframes/Keyframe/Title")
        # get all Track labels
        trackLabels = root.findall("./Tracks/Track/Label/Text")
        # get all tracks
        tracks = root.findall("./Tracks/Track")
    except:
        raise

    modify_select = 1
    has_file_been_modified = False
    err_stroke_no = []
    for lbl in trackLabels:
        fields = lbl.text.split(',')
        f = get_ints_from_time_string(fields[5])
        t = get_ints_from_time_string(time)
        if calculate_in_hundrendths(f[0], f[1], f[2], f[3]) < calculate_in_hundrendths(t[0] , t[1], t[2], t[3]) and f_leg == 'L':
            if (modify_select == 1):
                new_stroke_name = correct_stroke_sequence_L(fields)
                lbl.text = new_stroke_name
                has_file_been_modified = True
        elif calculate_in_hundrendths(f[0], f[1], f[2], f[3]) > calculate_in_hundrendths(t[0] , t[1], t[2], t[3]) and f_leg == 'L':
            if (modify_select == 1):
                new_stroke_name = correct_stroke_sequence_R(fields)
                lbl.text = new_stroke_name
                has_file_been_modified = True
        if calculate_in_hundrendths(f[0], f[1], f[2], f[3]) < calculate_in_hundrendths(t[0] , t[1], t[2], t[3]) and f_leg == 'R':
            if (modify_select == 1):
                new_stroke_name = correct_stroke_sequence_R(fields)
                lbl.text = new_stroke_name
                has_file_been_modified = True
        if calculate_in_hundrendths(f[0], f[1], f[2], f[3]) > calculate_in_hundrendths(t[0] , t[1], t[2], t[3]) and f_leg == 'R':
            if (modify_select == 1):
                new_stroke_name = correct_stroke_sequence_L(fields)
                lbl.text = new_stroke_name
                has_file_been_modified = True
    if has_file_been_modified == True:
        tree.write(filename + "2")




if __name__ == '__main__':
    main()
