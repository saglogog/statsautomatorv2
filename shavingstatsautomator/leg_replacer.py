#!/usr/bin/env python
import sys
import re
import json
import xml.etree.ElementTree as ET
from decimal import Decimal

def correct_stroke_sequence(fields):
    fields[3] = fields[3][:-1] + 'L'
    new_stroke_name = ",".join(fields)
    return new_stroke_name

def main():

    try:
        # get filename as argument
        filename = sys.argv[1]
        # parse
        tree = ET.parse(filename)

        # get root element
        root = tree.getroot()
        # get all keyframe titles
        keyframeTitles = root.findall("./Keyframes/Keyframe/Title")
        # get all Track labels
        trackLabels = root.findall("./Tracks/Track/Label/Text")
        # get all tracks
        tracks = root.findall("./Tracks/Track")
    except:
        raise

    modify_select = 1
    has_file_been_modified = False
    err_stroke_no = []
    for lbl in trackLabels:
        fields = lbl.text.split(',')
        if int(fields[0]) > 22:
            if (modify_select == 1):
                new_stroke_name = correct_stroke_sequence(fields)
                lbl.text = new_stroke_name
                has_file_been_modified = True
    if has_file_been_modified == True:
        tree.write(filename + "2")




if __name__ == '__main__':
    main()
