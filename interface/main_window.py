import logging
from kivy.logger import Logger
import kivy
kivy.require('1.10.0')  # replace with your current kivy version !

from kivy.app import App
from kivy.lang import Builder
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import ObjectProperty
from kivy.uix.popup import Popup
from kivy.factory import Factory
from kivy.uix.button import Button
from kivy.logger import Logger
from kivy.config import Config

import os
import xml.etree.ElementTree as ET

from shavingstatsautomator.reportcreator import ReportCreator
from shavingstatsautomator.customkvalabelvalidator import CustomKVALabelValidator
from shavingstatsautomator.phasescalculator import PhasesCalculator

# Load the non-main .kv files
Builder.load_file('interface/LoadDialog.kv')
Builder.load_file('interface/ErrorDialog.kv')


class LoadDialog(FloatLayout):
    '''
    The load dialog widget which is used in the load dialog popup.
    '''
    # adding widgets to widgets the python way
    # Kivy doc, chapter 12, Adding a background to a layout p. 94

    def __init__(self, **kwargs):
        super(LoadDialog, self).__init__(**kwargs)

        # add widget
        # self.add_widget(
        #     Button(
        #         text="Hello World",
        #         size_hint=(.5, .5),
        #         pos_hint={'center_x': .5, 'center_y': .5}))

    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


    def get_config_path(self):
        self.ids.filechooser.path = App.load_config().get(section = loadfile_path, option = path) 


class ErrorDialog(FloatLayout):
    '''
    The Error dialog widget which is used in the error dialog popup.
    '''

    def __init__(self, **kwargs):
        super(ErrorDialog, self).__init__(**kwargs)
        # Set the error_label label text to be what the calling method defines
        # it to be.
        self.ids.error_label.text = kwargs.pop('error_msg')

    cancel = ObjectProperty(None)


class MainWindow(FloatLayout):

    def __init__(self, **kwargs):
        # implement the functionality of the original class being overloaded
        super(MainWindow, self).__init__(**kwargs)
        self.fname = ''
        self.correct_str_seq = 0
        self.report_args = []

    def analyze_input(self):
        try:
            self.val = CustomKVALabelValidator(self.fname, self.tree)
            self.calc = PhasesCalculator(self.tree)
            self.rep = ReportCreator()

            # depending on what switch is active call the corresponding method to
            # print the report accordingly

            # validations
            if self.ids.show_phase_names.active == True:
                self.report_args.append(self.val.print_keyframe_titles())
            if self.ids.show_stroke_names.active == True:
                self.report_args.append(self.val.print_stroke_labels())
            if self.ids.val_phase_names.active == True:
                self.report_args.append(
                    self.val.validate_all_phase_names())
            if self.ids.val_stroke_names.active == True:
                self.report_args.append(
                    self.val.validate_all_stroke_names())
            if self.ids.cor_stroke_sequence.active == True:
                self.correct_str_seq = 1
            if self.ids.val_stroke_sequence.active == True:
                self.report_args.append(
                    self.val.validate_stroke_sequence(self.correct_str_seq))
            if self.ids.val_phase_times.active == True:
                self.report_args.append(self.val.validate_phase_times())
            if self.ids.val_stroke_times.active == True:
                self.report_args.append(self.val.validate_stroke_times())
            if self.ids.val_end_of_phases.active == True:
                self.report_args.append(self.val.validate_end_phases())

            # calculations
            if self.ids.shav_dur.active == True:
                self.report_args.append(
                    self.calc.find_shaving_phase_duration())
            if self.ids.time_rre.active == True:
                self.report_args.append(
                    self.calc.find_total_razor_rinsing_episodes())
            if self.ids.no_rre.active == True:
                self.report_args.append(
                    self.calc.find_number_of_rinsing_episodes())
            if self.ids.no_rre_types.active == True:
                self.report_args.append(
                    self.calc.find_how_many_of_each_type_of_rinsing_episode())
            if self.ids.tot_shav.active == True:
                self.report_args.append(
                    self.calc.find_total_actual_shaving())

            self.rep.print_report('test', self.report_args)
            # self.ids.RunButton.text = 'Pudding'

        except AttributeError:
            # if a file hasn't been loaded call show_errordialog()
            # to bring up the popup
            self.show_errordialog('You have to choose a file first!')

    def show_loadfile(self):
        # popup code
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load file",
                            content=content, size_hint=(0.9, 0.9))
        self._popup.open()

    def show_errordialog(self, error_message):
        content = ErrorDialog(cancel=self.dismiss_popup,
                              error_msg=error_message)
        self._popup = Popup(title="An Error Occured",
                            content=content, size_hint=(0.9, 0.9))
        self._popup.open()

    def display_settings(self, settings):
        try:
            p = self.settings_popup
        except AttributeError:
            self.settings_popup = Popup(content=settings, title='Settings',
                size_hint=(0.8, 0.8))
            p = self.settings_popup
        if p.content is not settings:
            p.content = settings
        p.open()

    def dismiss_popup(self):
        self._popup.dismiss()

    def load(self, path, filename):
        self.fname = filename
        with open(os.path.join(path, filename[0])) as stream:
            self.tree = ET.parse(stream)
        self._popup.dismiss()



class ShavingStatsAutomatorApp(App):
    def build_config(self, config):
        Logger.setLevel(logging.DEBUG)
        # config.setdefaults('kivy', {
        #     'log_level': 'warning',
        #     'log_enable': 1,
        #     'log_dir': 'log'
        # })
        config.setdefaults('loadfile_path', {
            'path': './'
        })
        # config.setdefaults('')

    def build_settings(self, settings):
        jsondata = """[
            { "type": "title",
            "title": "Test application" },

            { "type": "options",
            "title": "My first key",
            "desc": "Description of my first key",
            "section": "section1",
            "key": "key1",
            "options": ["value1", "value2", "another value"] },

            { "type": "numeric",
            "title": "My second key",
            "desc": "Description of my second key",
            "section": "section1",
            "key": "key2" }
        ]"""
        settings.add_json_panel('Test application', self.config, data=jsondata)

    def build(self):
        # Logger.setLevel(logging.DEBUG)
        return MainWindow()


if __name__ == '__main__':
    ShavingStatsAutomatorApp().run()
