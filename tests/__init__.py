import unittest2

class InitializationTests(unittest2.TestCase):

    def test_initialization(self):
        """
        Does the test suite run????
        """
        self.assertEqual(2+2, 4)

    def test_import(self):
        """
        Can the test suite import the module??
        """
        try:
            import shavingstatsautomator
        except ImportError:
            self.fail("Was not able to import shavingstatsautomator")
