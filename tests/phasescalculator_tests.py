import unittest2, json
from pkg_resources import ResourceManager
import xml.etree.ElementTree as ET
from shavingstatsautomator.phasescalculator import PhasesCalculator

class PhasesCalculatorTests(unittest2.TestCase):

    def setUp(self):
        rm = ResourceManager()
        filestream = rm.resource_stream("resources", "Giota_21_3.kva")
        tree = ET.parse(filestream)
        self.calc = PhasesCalculator(tree)

    def test_find_shaving_phase_duration(self):
        pass

    def test_find_total_razor_rinsing_episodes(self):
        pass

    def test_find_number_of_rinsing_episodes(self):
        """
        Does the program find the overall number of rinsing episodes correctly?
        """
        dec = json.JSONDecoder().decode(self.calc.find_number_of_rinsing_episodes())['No. of rinsing episodes (please validate phase names first)']
        self.assertEqual(str(dec), '3')

    def test_find_how_many_of_each_type_of_rinsing_episode(self):
        """
        Does the program find the # of each individual test correcty??
        """
        dec = json.JSONDecoder().decode(self.calc.find_how_many_of_each_type_of_rinsing_episode())["Rinsing Episode Types"]
        self.assertListEqual([dec[0]['Rinse Simple'], dec[1]['Rinse FR'], dec[2]['Rinse Knock']], [1, 0, 2])

    def test_find_total_actual_shaving(self):
        """
        Does the program calculate the actual shaving time correctly?
        """
        # print self.calc.find_total_actual_shaving()
        # self.fail()
        dec = json.JSONDecoder().decode(self.calc.find_total_actual_shaving())["Sum of stroke durations"]
        self.assertEqual(dec, '103')
