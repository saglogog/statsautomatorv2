import unittest2
import os.path
from pkg_resources import ResourceManager
import xml.etree.ElementTree as ET
from shavingstatsautomator.reportcreator import ReportCreator
from shavingstatsautomator.customkvalabelvalidator import CustomKVALabelValidator
from shavingstatsautomator.phasescalculator import PhasesCalculator

class ReportCreatorTests(unittest2.TestCase):

    def setUp(self):
        rm = ResourceManager()
        filestream = rm.resource_stream("resources", "Giota_21_3.kva")
        tree = ET.parse(filestream)
        self.val = CustomKVALabelValidator("Giota_21_3.kva", tree)
        self.calc = PhasesCalculator(tree)
        self.rep = ReportCreator()


    def test_print_report(self):
        """
        Is the file created correctly?
        """
        args = []
        args.append(self.val.print_keyframe_titles())
        args.append(self.val.print_stroke_labels())
        args.append(self.val.validate_all_phase_names())
        args.append(self.val.validate_all_stroke_names())
        args.append(self.val.validate_stroke_sequence(0))
        args.append(self.val.validate_stroke_times())
        args.append(self.val.validate_phase_times())
        args.append(self.val.validate_end_phases())
        args.append(self.calc.find_shaving_phase_duration())
        args.append(self.calc.find_total_razor_rinsing_episodes())
        args.append(self.calc.find_number_of_rinsing_episodes())
        args.append(self.calc.find_how_many_of_each_type_of_rinsing_episode())
        args.append(self.calc.find_total_actual_shaving())
        self.rep.print_report('test', args)
        self.assertTrue(os.path.isfile('./report_test.txt'))
