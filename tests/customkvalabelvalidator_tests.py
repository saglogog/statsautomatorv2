import unittest2, json, pkg_resources, pprint
import xml.etree.ElementTree as ET
from pkg_resources import ResourceManager
from shavingstatsautomator.customkvalabelvalidator import CustomKVALabelValidator

class CustomKVALabelValidatorTests(unittest2.TestCase):

    def setUp(self):
        rm = ResourceManager()
        # if not rm.resource_exists("resources", "Giota_21_3.kva"):
        #     self.fail("The requested resource doesn't exist")
        filestream = rm.resource_stream("resources", "Giota_21_3.kva")
        tree = ET.parse(filestream)
        self.val = CustomKVALabelValidator("Giota_21_3.kva", tree)

    def test_import_customkvalabelvalidator(self):
        """
        Can the test suite import the module??
        """
        try:
            from shavingstatsautomator.customkvalabelvalidator import CustomKVALabelValidator
        except ImportError:
            self.fail("Was not able to import CustomKVALabelValidator")

    def test_extract_phase_names(self):
        """
        Are the phase names extracted correctly from the list
        of the keyframe titles?
        """
        actualPhaseNames = self.val.extract_phase_names()
        expectedPhaseNames = ["ShavePr", "EndShavePr", "FoamApp", "EndFoamApp", "Shaving", "Rins", "EndRins",
        "Rins", "EndRins", "EndShaving", "FoamApp", "EndFoamApp", "Rins", "EndRins", "FoamRins", "FoamRins"]
        self.assertListEqual(actualPhaseNames, expectedPhaseNames)

    def test_validate_end_phases(self):
        """
        Does the program check for endphases correctly?
        """
        actualWrongPhases = json.JSONDecoder().decode(self.val.validate_end_phases())\
        ['Wrong End Phases (Please validate phase names and put elements in order first)']
        # Both the FoamRins phases are expected to be wrong as none have
        # corresponding ending phases
        expectedWrongPhases = ["FoamRins", "FoamRins"]
        self.assertListEqual(actualWrongPhases, expectedWrongPhases)

    def test_validate_phase_times(self):
        """
        Does the method find the duplicate times in phase text?
        """
        # if the code test fails, the print statement is printed in test output
        # thus to debug we print the desired output and fail the test
        # pprint.pprint(self.val.validate_phase_times())
        # self.fail('message')
        dec = json.JSONDecoder().decode(self.val.validate_phase_times())['Duplicate phase times']
        self.assertListEqual(dec, ['0:01:12:28', '0:01:12:28', '0:03:38:56'])


    def test_validate_stroke_times(self):
        """
        Does the method find the duplicate times in stroke text?
        """
        # pprint.pprint(self.val.validate_stroke_times())
        # self.fail('message')
        dec = json.JSONDecoder().decode(self.val.validate_stroke_times())['Duplicate stroke times']
        self.assertListEqual(dec, ['0:03:00:40', '0:03:55:88', '0:03:55:88'])
